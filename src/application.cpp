//
// Created by gonzalo on 11/23/17.
//
#include <iostream>
#include "LibraryA.h"

using namespace std;


int main(int argc, char *argv[]) {
    int result = DependantALibrary::LibraryA::Sum15(14);

    cout << "15 + 14 = " << result << endl;

    return EXIT_SUCCESS;
}

