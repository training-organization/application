from conans import ConanFile, tools


class ApplicationConan(ConanFile):
    name = "application"
    version = "0.1"
    license = "MIT"
    url = "https://bitbucket.org/ggarciatest/application"
    description = "Dependant of ggcommon and ggdepalib"
    settings = "os", "compiler", "build_type", "arch"
    requires = "libggcommon/0.1@demo/development", "ggdepalib/0.1@demo/development"
    generators = "cmake"

    def package(self):
        self.copy("*", dst="bin", src="build/bin")

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
