set(APPLICATION_NAME application)

cmake_minimum_required (VERSION 2.8)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(${APPLICATION_NAME}
        src/application.cpp
)

target_link_libraries(${APPLICATION_NAME}
        ${CONAN_LIBS}
)
