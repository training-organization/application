// Define some environment vars
def PACKAGE_NAME='application'
def PACKAGE_VERSION='0.1'
def PACKAGE_USER='demo'
def PACKAGE_CHANNEL='development'
def PACKAGE_LOCATOR = '${PACKAGE_NAME}/${PACKAGE_VERSION}@${PACKAGE_USER}/${PACKAGE_CHANNEL}'

pipeline {
    agent { label 'Windows  && CMake && VS2017' }
    options {
        buildDiscarder(logRotator(numToKeepStr: '15'))
    }
    stages {
        stage('Clean') {
            steps {
                bitbucketStatusNotify(
                    buildState: 'INPROGRESS',
                    buildKey: 'build',
                    buildName: 'Build'
                )
                dir('build') {
                    echo 'Remove old build directory'
                    deleteDir()
                }
            }
        }
        stage('Compile') {
            steps {
                checkout scm
                dir('build') {
                    echo 'Installing Conan Dependencies'
                    bat 'conan install ..'
                    echo 'Generating CMake Build Files'
                    bat 'cmake .. -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017"'
                    echo 'Compiling'
                    bat 'cmake --build . --config Release'
                }
            }
        }
        stage('Run Unit Tests') {
            steps {
                echo 'Building....'
            }
        }
        stage('Push to Conan') {
            when {
                branch 'master'
                branch 'develop'
            }

            steps {
                echo 'Creating conan Package'
                bat 'conan export-pkg . ${PACKAGE_LOCATOR}'
                echo 'Uploading Package to conan'
                bat 'conan upload ${PACKAGE_LOCATOR} --all --remote local'
            }
        }
    }
    post {
        success {
            bitbucketStatusNotify(
                buildState: 'SUCCESSFUL',
                buildKey: 'build',
                buildName: 'Build'
            )
        }
        failure {
            bitbucketStatusNotify(
                buildState: 'FAILED',
                buildKey: 'build',
                buildName: 'Build',
                buildDescription: 'Something went wrong with tests!'
            )
        }
    }
}
